
## Capítulo I - BASES DE LA INSTITUCIONALIDAD
#### Artículo 1° 
Las personas nacen libres e iguales en dignidad y derechos.

La [familia](#familia) es el núcleo fundamental de la sociedad.

El Estado reconoce y ampara a los grupos intermedios a través de los cuales se organiza y estructura la sociedad y les garantiza la adecuada autonomía para cumplir sus propios fines específicos.

El Estado está al servicio de la persona humana y su finalidad es promover el bien común, para lo cual debe contribuir a crear las condiciones sociales que permitan a todos y a cada uno de los integrantes de la comunidad nacional su mayor realización espiritual y material posible, con pleno respeto a los derechos y garantías que esta Constitución establece.

Es deber del Estado resguardar la seguridad nacional, dar protección a la población y a la familia, propender al fortalecimiento de ésta, promover la integración armónica de todos los sectores de la Nación y asegurar el derecho de las personas a participar con igualdad de oportunidades en la vida nacional.

> Tean: en asegurar yo agregaría: "asegurar la entrega de bienes fundamentales en forma gratuita y de calidad para todos los miembros de la nación, así como también el derecho de las personas...". Los bienes fundamentales los definiría en otra parte: Educación y Salud

> Fuman: Creo que debería hacer referencia a individuos sin referirse específicamente a familia

#### Artículo 2º
Son emblemas nacionales la bandera nacional, el escudo de armas de la República y el himno nacional.

#### Artículo 3º
El Estado de Chile es unitario.

La administración del Estado será funcional y territorialmente descentralizada, o desconcentrada en su caso, de conformidad a la ley.

Los órganos del Estado promoverán el fortalecimiento de la regionalización del país y el desarrollo equitativo y solidario entre las regiones, provincias y comunas del territorio nacional.


#### Artículo 4°
Chile es una república democrática.

#### Artículo 5º
La soberanía reside esencialmente en la Nación. Su ejercicio se realiza por el pueblo a través del plebiscide elecciones periódicas y, también, por las autoridades que esta Constitución establece. Ningún sector del pueblindividuo alguno puede atribuirse su ejercicio.
El ejercicio de la soberanía reconoce como limitación el respeto a los derechos esenciales que emanan de la naturaleza humEs deber de los órganos del Estado respetar y promover tales derechos, garantizados por esta Constitución, así como portratados internacionales ratificados por Chile y que se encuentren vigentes.




## Glosario

- <a name="familia">Familia:</a> TODO: Definir